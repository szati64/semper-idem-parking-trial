package hu.si.szati.trial.parking.web.views;

import hu.si.szati.trial.parking.client.api.service.car.CarServiceRemote;
import hu.si.szati.trial.parking.client.api.service.parking.ParkingServiceRemote;
import hu.si.szati.trial.parking.client.api.vo.CarVo;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ViewScoped
@ManagedBean(name = "carTableView")
public class CarTableView {

    private static final Logger LOG = Logger.getLogger(CarTableView.class.getName());

    private List<CarVo> cars;

    @EJB
    private CarServiceRemote carServiceRemote;

    @EJB
    private ParkingServiceRemote parkingServiceRemote;
    
    @ManagedProperty(value="#{addCarView}")
    private AddCarView addCarView;

    @PostConstruct
    public void init() {
        cars = carServiceRemote.findAll();
    }

    public void addCarToList() {
        LOG.trace("add car view save called");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        CarVo car = addCarView.getCar();
        
        if (carServiceRemote.findByPlateNumber(car.getLicensePlateNumber()) != null) {
            requestContext.execute("PF('addCarErrorDialog').show();");
            addCarView.setCar(new CarVo());
            requestContext.execute("PF('addCarDialog').hide();");
            requestContext.update("addCarForm");
            return;
        }

        LOG.trace("saving...");
        carServiceRemote.save(car);
        cars.add(car);
        addCarView.setCar(new CarVo());

        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Autó sikeresen hozzáadva", null));
        requestContext.execute("PF('addCarDialog').hide();");
        requestContext.update("addCarForm");
    }

    public void deleteRow(CarVo car) {
        LOG.debug(car + " deleteing...");
        FacesContext facesContext = FacesContext.getCurrentInstance();

        if (parkingServiceRemote.findActiveParkingByPlateNumber(car.getLicensePlateNumber()) != null) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Törlés sikertelen, az autó parkolóban van!", null));
            return;
        }

        carServiceRemote.deleteByPlateNumber(car.getLicensePlateNumber());
        cars.removeIf(x -> x.getLicensePlateNumber().equals(car.getLicensePlateNumber()));
        LOG.trace("find all: " + carServiceRemote.findAll());
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Autó törlése sikeres", null));
    }

    public List<CarVo> getCars() {
        return cars;
    }

    public void setCars(List<CarVo> cars) {
        this.cars = cars;
    }

    public CarServiceRemote getCarServiceRemote() {
        return carServiceRemote;
    }

    public void setCarServiceRemote(CarServiceRemote carServiceRemote) {
        this.carServiceRemote = carServiceRemote;
    }

    public ParkingServiceRemote getParkingServiceRemote() {
        return parkingServiceRemote;
    }

    public void setParkingServiceRemote(ParkingServiceRemote parkingServiceRemote) {
        this.parkingServiceRemote = parkingServiceRemote;
    }

    public AddCarView getAddCarView() {
        return addCarView;
    }

    public void setAddCarView(AddCarView addCarView) {
        this.addCarView = addCarView;
    }

}
