package hu.si.szati.trial.parking.web.views;

import hu.si.szati.trial.parking.client.api.service.car.CarServiceRemote;
import hu.si.szati.trial.parking.client.api.service.parking.ParkingServiceRemote;
import hu.si.szati.trial.parking.client.api.vo.CarVo;
import hu.si.szati.trial.parking.client.api.vo.ParkingVo;
import java.util.Date;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SlideEndEvent;
import org.primefaces.event.TabChangeEvent;

@ViewScoped
@ManagedBean(name = "parkingView")
public class ParkingView {

    private static final Logger LOG = Logger.getLogger(ParkingView.class.getName());
    private static final long HOUR_IN_MILLISEC = 3600 * 1000;

    private CarVo selectedCar;
    private Integer parking;
    private Integer toHours;

    @EJB
    private CarServiceRemote carServiceRemote;
    
    @EJB
    private ParkingServiceRemote parkingServiceRemote;

    @PostConstruct
    public void init() {
        parking = 0;
        toHours = 1;
    }

    public void save() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        
        if (parkingServiceRemote.findActiveParkingByPlateNumber(selectedCar.getLicensePlateNumber()) != null) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Az autó már parkolóban van!", null));
            return;
        }

        ParkingVo parkingVo = new ParkingVo();
        parkingVo.setLicensePlateNumber(selectedCar.getLicensePlateNumber());
        Date now = new Date();
        parkingVo.setFrom(now);
        Date to = new Date(new Date().getTime() + HOUR_IN_MILLISEC * toHours);
        parkingVo.setTo(to);
        parkingVo.setParkingNumber(parking);

        LOG.trace("saving" + parkingVo);
        parkingServiceRemote.save(parkingVo);
        LOG.trace(parkingServiceRemote.findActiveParkingByPlateNumber(selectedCar.getLicensePlateNumber()));
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Autó sikeresen parkolóba helyezve!", null));
        requestContext.execute("PF('parkingDialog').hide();");
    }

    public void onTabChange(TabChangeEvent event) {
        switch (event.getTab().getTitle()) {
            case "Parkoló 1":
                parking = 0;
                break;
            case "Parkoló 2":
                parking = 1;
                break;
            case "Parkoló 3":
                parking = 2;
                break;
        }
    }

    public void onSlideEnd(SlideEndEvent event) {
        toHours = event.getValue();
    }

    public CarVo getSelectedCar() {
        return selectedCar;
    }

    public void setSelectedCar(CarVo selectedCar) {
        this.selectedCar = selectedCar;
    }

    public Integer getParking() {
        return parking;
    }

    public void setParking(Integer parking) {
        this.parking = parking;
    }

    public Integer getToHours() {
        return toHours;
    }

    public void setToHours(Integer toHours) {
        this.toHours = toHours;
    }

}
