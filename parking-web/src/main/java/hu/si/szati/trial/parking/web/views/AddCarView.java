package hu.si.szati.trial.parking.web.views;

import hu.si.szati.trial.parking.client.api.vo.CarVo;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

@ViewScoped
@ManagedBean(name = "addCarView")
public class AddCarView {

    private static final Logger LOG = Logger.getLogger(AddCarView.class.getName());

    private CarVo car;

    @PostConstruct
    public void init() {
        car = new CarVo();
    }

    public CarVo getCar() {
        return car;
    }

    public void setCar(CarVo car) {
        this.car = car;
    }
}
