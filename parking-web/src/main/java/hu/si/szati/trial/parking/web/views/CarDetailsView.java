package hu.si.szati.trial.parking.web.views;

import hu.si.szati.trial.parking.client.api.service.car.CarServiceRemote;
import hu.si.szati.trial.parking.client.api.service.parking.ParkingServiceRemote;
import hu.si.szati.trial.parking.client.api.vo.CarVo;
import hu.si.szati.trial.parking.client.api.vo.ParkingVo;
import java.util.Date;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ViewScoped
@ManagedBean(name = "carDetailsView")
public class CarDetailsView {

    private static final Logger LOG = Logger.getLogger(CarDetailsView.class.getName());
    
    @EJB
    private CarServiceRemote carServiceRemote;

    @EJB
    private ParkingServiceRemote parkingServiceRemote;

    private CarVo car;
    private ParkingVo activeParking;

    @ManagedProperty(value="#{parkingView}")
    private ParkingView parkingView;
    
    @PostConstruct
    public void init() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map<String, String> params = facesContext.getExternalContext().getRequestParameterMap();

        car = carServiceRemote.findByPlateNumber((String) params.get("carPlateNumber"));
        activeParking = parkingServiceRemote.findActiveParkingByPlateNumber(car.getLicensePlateNumber());
        
        parkingView.setSelectedCar(car);
    }
    
    public void saveParking() {
        parkingView.save();
        car = carServiceRemote.findByPlateNumber(car.getLicensePlateNumber());
        activeParking = parkingServiceRemote.findActiveParkingByPlateNumber(car.getLicensePlateNumber());
    }

    public CarServiceRemote getCarServiceRemote() {
        return carServiceRemote;
    }

    public void setCarServiceRemote(CarServiceRemote carServiceRemote) {
        this.carServiceRemote = carServiceRemote;
    }

    public ParkingServiceRemote getParkingServiceRemote() {
        return parkingServiceRemote;
    }

    public void setParkingServiceRemote(ParkingServiceRemote parkingServiceRemote) {
        this.parkingServiceRemote = parkingServiceRemote;
    }

    public CarVo getCar() {
        return car;
    }

    public void setCar(CarVo car) {
        this.car = car;
    }

    public ParkingVo getActiveParking() {
        return activeParking;
    }

    public void setActiveParking(ParkingVo activeParking) {
        this.activeParking = activeParking;
    }

    public ParkingView getParkingView() {
        return parkingView;
    }

    public void setParkingView(ParkingView parkingView) {
        this.parkingView = parkingView;
    }

}
