package hu.si.szati.trial.parking.service.startup;

import hu.si.szati.trial.parking.client.api.service.car.CarServiceRemote;
import hu.si.szati.trial.parking.client.api.vo.CarVo;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.apache.log4j.Logger;

@Startup
@Singleton
public class StartUpBean {

    private static final Logger LOG = Logger.getLogger(StartUpBean.class.getName());
    
    @EJB
    CarServiceRemote carServiceRemote;
    
    @PostConstruct
    public void init() {
        CarVo carVo1 = new CarVo();
        carVo1.setBrand("Nissan");
        carVo1.setColor("black");
        carVo1.setType("370z");
        carVo1.setLicensePlateNumber("ASD-123");
        carServiceRemote.save(carVo1);
        LOG.trace(String.format("%s added to the database", carVo1));
        LOG.trace(String.format("%s", carServiceRemote.findAll()));
    }
}
