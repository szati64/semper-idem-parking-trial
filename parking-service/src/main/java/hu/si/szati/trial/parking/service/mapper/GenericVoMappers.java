package hu.si.szati.trial.parking.service.mapper;

import hu.si.szati.trial.parking.client.api.vo.CarVo;
import hu.si.szati.trial.parking.client.api.vo.ParkingVo;
import hu.si.szati.trial.parking.data.entity.CarEntity;
import hu.si.szati.trial.parking.data.entity.ParkingEntity;

public final class GenericVoMappers {
	public static final GenericVoMapper<CarEntity, CarVo> CAR_VO_MAPPER;
        public static final GenericVoMapper<ParkingEntity, ParkingVo> PARKING_VO_MAPPER;
	
	static {
            CAR_VO_MAPPER = new GenericVoMapper<>(CarEntity.class, CarVo.class);
            PARKING_VO_MAPPER = new GenericVoMapper<>(ParkingEntity.class, ParkingVo.class);
	}
	
	private GenericVoMappers() {}
}
