package hu.si.szati.trial.parking.service.impl;

import hu.si.szati.trial.parking.client.api.service.parking.ParkingServiceLocal;
import hu.si.szati.trial.parking.client.api.service.parking.ParkingServiceRemote;
import hu.si.szati.trial.parking.client.api.vo.ParkingVo;
import hu.si.szati.trial.parking.data.database.ParkingsInDatabase;
import hu.si.szati.trial.parking.data.entity.ParkingEntity;
import hu.si.szati.trial.parking.service.mapper.GenericVoMappers;
import java.util.Date;
import java.util.Optional;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import org.apache.log4j.Logger;

@Stateless
@Local(ParkingServiceLocal.class)
@Remote(ParkingServiceRemote.class)
public class ParkingServiceBean implements ParkingServiceLocal, ParkingServiceRemote {

    private static final Logger LOG = Logger.getLogger(ParkingServiceBean.class.getName());
    
    @Override
    public ParkingVo findActiveParkingByPlateNumber(String plateNumber) {
        checkParkings();
        
        Optional<ParkingEntity> result = ParkingsInDatabase.parkingsInDatabase.stream()
                .filter(x -> x.getLicensePlateNumber().equals(plateNumber))
                .findFirst();
        
        return result.isPresent() ? GenericVoMappers.PARKING_VO_MAPPER.toVo(result.get()) : null;
    }
    
    @Override
    public ParkingVo save(ParkingVo parkingVo) {
        ParkingEntity parkingEntity = GenericVoMappers.PARKING_VO_MAPPER.toEntity(parkingVo);
        LOG.trace("entity: " + parkingEntity);
        ParkingsInDatabase.parkingsInDatabase.add(parkingEntity);
        LOG.trace(ParkingsInDatabase.parkingsInDatabase);
        return parkingVo;
    }
    
    private void checkParkings() {
        LOG.trace("before check: " + ParkingsInDatabase.parkingsInDatabase);
        Date now = new Date();
        ParkingsInDatabase.parkingsInDatabase.removeIf(x -> now.after(x.getTo()));
        LOG.trace("after check: " + ParkingsInDatabase.parkingsInDatabase);
    }

}
