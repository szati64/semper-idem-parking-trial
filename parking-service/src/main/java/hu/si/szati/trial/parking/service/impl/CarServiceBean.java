package hu.si.szati.trial.parking.service.impl;

import hu.si.szati.trial.parking.client.api.service.car.CarServiceLocal;
import hu.si.szati.trial.parking.client.api.service.car.CarServiceRemote;
import hu.si.szati.trial.parking.client.api.vo.CarVo;
import hu.si.szati.trial.parking.data.database.CarsInDatabase;
import hu.si.szati.trial.parking.data.entity.CarEntity;
import hu.si.szati.trial.parking.service.mapper.GenericVoMappers;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;

@Stateless
@Local(CarServiceLocal.class)
@Remote(CarServiceRemote.class)
public class CarServiceBean implements CarServiceLocal, CarServiceRemote {

    @Override
    public CarVo findByPlateNumber(String plateNumber) {
        for (CarEntity carEntity : CarsInDatabase.carsInDatabase) {
            if (carEntity.getLicensePlateNumber().equals(plateNumber)) {
                return GenericVoMappers.CAR_VO_MAPPER.toVo(carEntity);
            }
        }
        return null;
    }

    @Override
    public List<CarVo> findAll() {
        return GenericVoMappers.CAR_VO_MAPPER.toVo(CarsInDatabase.carsInDatabase);
    }

    @Override
    public CarVo save(CarVo carVo) {
        CarEntity carEntity = GenericVoMappers.CAR_VO_MAPPER.toEntity(carVo);
        CarsInDatabase.carsInDatabase.add(carEntity);
        return carVo;
    }

    @Override
    public void delete(CarVo carVo) {
        CarsInDatabase.carsInDatabase.removeIf(x -> x.getLicensePlateNumber().equals(carVo.getLicensePlateNumber()));
    }

    @Override
    public void deleteByPlateNumber(String plateNumber) {
        CarsInDatabase.carsInDatabase.removeIf(x -> x.getLicensePlateNumber().equals(plateNumber));
    }

}
