package hu.si.szati.trial.parking.client.api.service.parking;

import hu.si.szati.trial.parking.client.api.vo.ParkingVo;

public interface ParkingService {
    ParkingVo findActiveParkingByPlateNumber(String plateNumber);
    ParkingVo save(ParkingVo parkingVo);
}
