package hu.si.szati.trial.parking.client.api.service.car;

import hu.si.szati.trial.parking.client.api.vo.CarVo;
import java.util.List;

public interface CarService {
    CarVo findByPlateNumber(String plateNumber);
    List<CarVo> findAll();
    CarVo save(CarVo carVo);
    void delete(CarVo carVo);
    void deleteByPlateNumber(String plateNumber);
}
