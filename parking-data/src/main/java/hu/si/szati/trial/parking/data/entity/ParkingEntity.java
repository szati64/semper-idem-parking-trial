package hu.si.szati.trial.parking.data.entity;

import java.util.Date;

public class ParkingEntity {
    private String licensePlateNumber;
    private Date from;
    private Date to;
    private Integer parkingNumber;

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public void setLicensePlateNumber(String licensePlateNumber) {
        this.licensePlateNumber = licensePlateNumber;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Integer getParkingNumber() {
        return parkingNumber;
    }

    public void setParkingNumber(Integer parkingNumber) {
        this.parkingNumber = parkingNumber;
    }

    @Override
    public String toString() {
        return "ParkingEntity{" + "licensePlateNumber=" + licensePlateNumber + ", from=" + from + ", to=" + to + ", parkingNumber=" + parkingNumber + '}';
    }

}
