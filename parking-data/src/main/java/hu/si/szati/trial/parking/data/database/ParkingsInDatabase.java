package hu.si.szati.trial.parking.data.database;

import hu.si.szati.trial.parking.data.entity.ParkingEntity;
import java.util.ArrayList;
import java.util.List;

public final class ParkingsInDatabase {

    public static final int NUMBER_OF_PARKING_PLACES = 3;
    public static List<ParkingEntity> parkingsInDatabase;

    static {
        parkingsInDatabase = new ArrayList();
    }

    private ParkingsInDatabase() {
    }

}
