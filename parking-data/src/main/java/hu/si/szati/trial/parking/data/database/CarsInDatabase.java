package hu.si.szati.trial.parking.data.database;

import hu.si.szati.trial.parking.data.entity.CarEntity;
import java.util.ArrayList;
import java.util.List;

public final class CarsInDatabase {

    public static List<CarEntity> carsInDatabase = new ArrayList<>();
    
    private CarsInDatabase() {
    }
    
}
