package hu.si.szati.trial.parking.data.entity;

public class CarEntity {
    private String brand;
    private String type;
    private String licensePlateNumber;
    private String color;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public void setLicensePlateNumber(String licensePlateNumber) {
        this.licensePlateNumber = licensePlateNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "CarEntity{" + "brand=" + brand + ", type=" + type + ", licensePlateNumber=" + licensePlateNumber + ", color=" + color + '}';
    }

}
